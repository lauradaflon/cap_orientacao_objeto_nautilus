# Guide of use #

**How can I use the program?**
You can download the file, then, in the directory you downloaded, go to the terminal and type `python3 championship.py`. It's important to type `python3` and not just `python`, because this code use functions of python3.

**What the program does?**
It simulates a Hogwarts championship between the houses. It has pre-stipulated points, and prints them in the right order. After that, you can choose the house you want to know more about, just by typing the name in the terminal (it doesn't matter the letters are upper or lowercase).