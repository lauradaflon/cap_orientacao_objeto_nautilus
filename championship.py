class Hogwarts:
    def __init__(self,house,points,colors,animal,founder):
        self.house = house
        self.points = points
        self.colors = colors
        self.animal = animal
        self.founder = founder

    def __gt__(self, other):
        return self.points > other.points

    def __lt__(self, other):
        return self.points < other.points

    def __eq__(self,other):
        return self.points == other.points

    def __add__(self, other):
        return self.points + other.points

    def __sub__(self, other):
        return self.points - other.points

    def __repr__(self):
        points = str(self.points)
        return ('\nThe choosen house was ' + self.house + ', which made ' + points + ' points.\nThe house colors are ' + self.colors + ' and the animal is ' + self.animal + '. The founder is ' + self.founder +'.\n')


Gryffindor = Hogwarts('Gryffindor',245,'Gold and Red','Lion','Godric Gryffindor')
Hufflepuff = Hogwarts('Hufflepuff',350,'Yellow and Black','Badger','Helga Hufflepuff')
Ravenclaw = Hogwarts('Ravenclaw',210,'Blue and Bronze','Eagle','Rowena Ravenclaw')
Slytherin = Hogwarts('Slytherin',230,'Green and Silver','Serpent','Salazar Slytherin')

l = [Gryffindor.points,Hufflepuff.points,Ravenclaw.points,Slytherin.points]
l.sort()
print(l)
print('Sum of Gryffindor and Hufflepuff: ' + str(Gryffindor+Hufflepuff))

ans = 'y'
while ans == 'Y' or ans == 'y':
    house = (input('Do you want to know about what house?  ')).casefold()
    if house == 'hufflepuff': print(Hufflepuff)
    if house == 'ravenclaw': print(Ravenclaw)
    if house == 'slytherin' : print(Slytherin)
    if house == 'gryffindor': print(Gryffindor)
    if house != 'hufflepuff' and house != 'ravenclaw' and house != 'slytherin' and house != 'gryffindor': print('Invalid input')
    ans = input('Do you want to continue? (Y/n)  ')
